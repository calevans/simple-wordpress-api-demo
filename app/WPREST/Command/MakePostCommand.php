<?PHP
namespace WPREST\Command;

use WPREST\Traits\GetToken;

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use \GuzzleHttp\Client;

/**
 * Sample code for Using the WordPress REST API
 *
 * This class shows a simple workflow to accomplish the following
 *   - Authenticate
 *   - Create a new post
 *   - Upload an image
 *   - Update the post by setting the featured image and setting status to
 *     publish.
 *
 * It's primary purpose is to give concrete examples for creating and
 * updating posts and media.
 */

class MakePostCommand extends Command
{
  use GetToken;

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [];

      $this->setName('post')
           ->setDescription('Create a post')
           ->setDefinition($definition)
           ->setHelp("Reads a text file in the data dir and creates a new post from it.");
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Create a Post via the WordPress API', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();

    $this->getToken($this->debug);
    $client  = new Client();

    $payload = $this->readDataFile(
      $this->getApplication()->config['basepath'] . 'data/post.txt'
    );

    /*
     * Create the post
     */
    $response = $client->request(
      'POST',
      $this->getApplication()->config['baseurl'] . '/wp/v2/posts',
      ['json' => $payload,
        'headers' => [
          'Authorization' => 'Bearer ' . $this->token['token'],
          'Accept'        => 'application/json',
          'Content-type'  => 'application/json',
          ],
        'debug' => $this->debug
      ]
    );
    $post = json_decode($response->getBody()->getContents());

    /*
     * Upload the image
     */
    $rawBaseName = 'image.jpg';
    $file = fopen(
      $this->getApplication()->config['basepath'] . 'data/' . $rawBaseName ,
      'r'
    );

    $response = $client->request(
      'POST',
      $this->getApplication()->config['baseurl'] . '/wp/v2/media',
      [
      'body'    => $file,
      'debug'   => $this->debug,
      'headers' => [
        'Accept'              => 'application/json',
        'Cache-Control'       => 'no-cache',
        'Content-type'        => 'image/jpeg',
        'Content-Disposition' => 'form-data; filename="' . $rawBaseName . '"',
        'Authorization'       => 'Bearer ' . $this->token['token']
        ]
      ]
    );
    $picture = json_decode($response->getBody()->getContents());

    /*
     * Update the post with the image and set it to published
     */
    $payload = [
      'featured_media' => $picture->id,
      'status'         => 'publish'
    ];

    $response = $client->request(
      'PATCH',
      $post->_links->self[0]->href,
      ['json' => $payload,
        'headers' => [
          'Authorization' => 'Bearer ' . $this->token['token'],
          'Accept'        => 'application/json',
          'Content-type'  => 'application/json',
          ],
        'debug' => $this->debug
      ]
    );

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

  /**
   * Reads the text file into an array and then breaks each element into it's
   * key and value.
   *
   * @param string $file_name The name of the file to read.
   * @return array
   */
  protected function readDataFile(string $file_name) : array {
    $payload = [];
    $raw_payload = file( $file_name );

    foreach( $raw_payload as $raw_line ) {
      if (! empty( $raw_line )) {
        $holding = explode( '=' , $raw_line );
        $payload[trim($holding[0])] = trim($holding[1]);
      }
    }

    $payload['title'] .= ' ' . date('Y-m-d h:i:s'); // Make the title unique
    return $payload;
  }

}