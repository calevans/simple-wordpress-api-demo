<?PHP
namespace WPREST\Command;

use WPREST\Traits\GetToken;

use Symfony\Component\Console\ {
  Input\InputInterface,
  Input\InputOption,
  Input\InputArgument,
  Output\OutputInterface,
  Command\Command
};

use \GuzzleHttp\Client;

/**
 * Sample code for Using the WordPress REST API
 *
 * This is a simple example to show show how to fetch a filtered user list
 * and update the currently logged in user.
 *
 */

class UserManagementCommand extends Command
{
  use GetToken;

  protected $debug = false;

  /**
   * Called by the application, this method sets up the command.
   */
  protected function configure()
  {
      $definition = [];

      $this->setName('user')
           ->setDescription('Update an existing user')
           ->setDefinition($definition)
           ->setHelp("Update the description of an existing user with a counter of the number of times it has been updated.");
      return;
  }

  /**
   * Main body of this command
   *
   * @param InputInterface $input
   * @param OutputInterface $output
   */
  public function execute(InputInterface $input, OutputInterface $output)
  {
    $output->writeln('Update a user using the WordPress API', OutputInterface::VERBOSITY_NORMAL);
    $this->debug = $output->isDebug();
    $this->getToken($this->debug);
    $client  = new Client();

    /*
     * Get the record of the logged in user
     */
    $response = $client->request(
      'GET',
      $this->getApplication()->config['baseurl'] . '/wp/v2/users/?slug=' . $this->token['user_nicename'] . '&context=edit',
      [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->token['token'],
          'Accept'        => 'application/json',
          'Content-type'  => 'application/json',
          ],
        'debug' => $this->debug
      ]
    );
    $user = json_decode($response->getBody()->getContents(),true)[0];

    /*
     * Increment the counter and store in description
     */
    $counter = (int)$user['description'];
    $user['description'] = (string)(++$counter); // It HAS to be a string

    /*
     * Push it back to the API
     */
    try {
      $client->request(
        'PATCH',
        $user['_links']['self'][0]['href'],
        [
          'json' => ['description' => $user['description']],
          'headers' => [
            'Authorization' => 'Bearer ' . $this->token['token'],
            'Accept'        => 'application/json',
            'Content-type'  => 'application/json',
            ],
          'debug' => $this->debug
        ]
      );
    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
        $response = json_decode($e->getResponse()->getBody()->getContents(),true);
        $output->writeln("An Error has occurred!\n" . print_r($response,true));
    }

    $output->writeln('Done' , OutputInterface::VERBOSITY_NORMAL);
  }

}