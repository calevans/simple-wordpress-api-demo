# Simple WordPress API Demo

This is a simple demonstration program on how to use the WordPress REST API to create and edit posts.

It is part of ["Using the WordPress REST API"](https://leanpub.com/wordpress_rest_api_i) by Cal Evans

### Steps to get the code working
**This repo requires PHP 7.3+**

**This project requires composer be installed and accessible**

1. Fork the repo into your own copy
2. Clone your copy of the repo to your local machine
3. In the root of the project execute the following
```bash
  $ composer install
```

4. Rename `config/config.sample` to `config/config.php`
5. Open config.php in an editor and enter in the needed values.
  - username
  - password
  - the root url for the REST API on your test WordPress installation e.g. `https://example.com/wp-json`
6. Save config.php
7. Change directories to the app directory
8. Execute the following

```bash
  $ ./console validate
```
